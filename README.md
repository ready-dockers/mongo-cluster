# MongoDB Cluster

Mongo DB - 3 node cluster.  
1 Primary and 2 secondary nodes.

node1:27017 exposed on localhost:27017  
node2:27017 exposed on localhost:27018  
node3:27017 exposed on localhost:27019  