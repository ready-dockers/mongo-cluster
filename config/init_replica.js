config = {
    _id: "mongo-cluster", members: [
        { _id: 0, host: "node1:27017", priority: 1 },
        { _id: 1, host: "node2:27017", priority: 0.5 },
        { _id: 2, host: "node3:27017", priority: 0.5 }
    ]
};

rs.initiate(config);
rs.status();